apt update
apt upgrade -y

apt install -y vim
apt install -y git
apt install -y docker.io

apt install -y nodejs
apt install -y npm
npm i -g npm@latest
npm i -g n stable

apt install ruby

git config user.email "whcxar@gmail.com"
git config --global user.name "whcxar"
